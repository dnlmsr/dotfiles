# Dotfiles

This is a collection of my dotfiles. Use [yadm](https://yadm.io) if you want them nicely managed.

## Usage

You may need `ansible` and `ansible-core` in order to bootsrap the system.
You can download the dotfiles using the following command:

```sh
# HTTPS connection
yadm clone -b work https://github.com/dnlmsr/dotfiles.git

# SSH connection
yadm clone -b work git@github.com:dnlmsr/dotfiles.git

yadm bootstrap
```
