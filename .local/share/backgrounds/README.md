# Backgrounds

## Disclaimer

I do not own these wallpapers. It's handy for me to have my wallpapers tracked with Git.

## Attributions

If any of these author attributions or links are incorrect in some way, please feel free to contact me or submit a pull request.

- [Minimalist Desert at Sunset Wallpaper](https://wallpapersden.com/minimalist-desert-at-sunset-wallpaper/3840x2160/)
