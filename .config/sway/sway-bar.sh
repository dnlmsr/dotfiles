#!/bin/sh
date_formatted=$(date +'%Y-%m-%d %H:%M:%S')

battery_status=$(acpi -b)

echo 󰁹 $battery_status 󰥔 $date_formatted
