local lazypath = vim.fn.stdpath("data") .. "/lazy/lazy.nvim"
if not vim.loop.fs_stat(lazypath) then
	local lazyrepo = "https://github.com/folke/lazy.nvim.git"
	vim.fn.system({ "git", "clone", "--filter=blob:none", "--branch=stable", lazyrepo, lazypath })
end
vim.opt.rtp:prepend(lazypath)

require("lazy").setup({
	{ "numToStr/Comment.nvim", opts = {} },
	{ "ledger/vim-ledger", lazy = false },
	{
		"stevearc/oil.nvim",
		opts = {},
		dependencies = { "nvim-tree/nvim-web-devicons" },
		keys = { { "<leader>ad", "<cmd>Oil<cr>", desc = "file manager" } },
		config = function()
			require("oil").setup()
		end,
	},
	{
		"windwp/nvim-autopairs",
		event = "InsertEnter",
		config = true,
		opts = {},
	},
	{
		"folke/which-key.nvim",
		event = "VimEnter",
		config = function()
			require("which-key").setup()
			require("which-key").add({
				{ "<leader>a", group = "applications" },
				{ "<leader>a_", hidden = true },
				{ "<leader>b", group = "buffer" },
				{ "<leader>b_", hidden = true },
				{ "<leader>f", group = "file" },
				{ "<leader>f_", hidden = true },
				{ "<leader>l", group = "lsp" },
				{ "<leader>l_", hidden = true },
				{ "<leader>p", group = "project" },
				{ "<leader>p_", hidden = true },
				{ "<leader>t", group = "format" },
				{ "<leader>t_", hidden = true },
				{ "<leader>w", group = "windows" },
				{ "<leader>w_", hidden = true },
			})
		end,
	},
	{
		"nvim-telescope/telescope.nvim",
		event = "VimEnter",
		branch = "0.1.x",
		dependencies = {
			{
				"ahmedkhalf/project.nvim",
				config = function()
					require("project_nvim").setup({})
				end,
			},
			"nvim-lua/plenary.nvim",
			{
				"nvim-telescope/telescope-fzf-native.nvim",
				build = "make",
				cond = function()
					return vim.fn.executable("make") == 1
				end,
			},
			{ "nvim-tree/nvim-web-devicons", enabled = vim.g.have_nerd_font },
		},
		config = function()
			require("telescope").setup({})

			local builtin = require("telescope.builtin")

			pcall(require("telescope").load_extension, "fzf")
			pcall(require("telescope").load_extension, "ui-select")
			pcall(require("telescope").load_extension, "projects")

			vim.keymap.set("n", "<leader>ff", builtin.find_files, { desc = "find file" })
			vim.keymap.set("n", "<leader>fr", builtin.oldfiles, { desc = "find recent" })

			vim.keymap.set("n", "<leader>pf", builtin.git_files, { desc = "find file in project" })
			vim.keymap.set(
				"n",
				"<leader>pp",
				require("telescope").extensions.projects.projects,
				{ desc = "Find project" }
			)
			vim.keymap.set("n", "<leader>ps", function()
				builtin.grep_string({ search = vim.fn.input("Grep > ") })
			end, { desc = "Grep in project" })

			vim.keymap.set("n", "<leader>bb", builtin.buffers, { desc = "find buffer" })
		end,
	},
	{
		"stevearc/conform.nvim",
		lazy = false,
		keys = {
			{
				"<leader>tt",
				function()
					require("conform").format({ async = true, lsp_fallback = true })
				end,
				mode = "",
				desc = "Format buffer",
			},
		},
		opts = {
			notify_on_error = true,
			format_on_save = { timeout_ms = 500, lsp_fallback = true },
			formatters = {
				dockfmt = { command = "dockfmt", args = { "fmt" } },
				black = {
					inherit = true,
					args = { "--preview", "--line-length=79", "--stdin-filename", "$FILENAME", "--quiet", "-" },
				},
			},
			formatters_by_ft = {
				c = { "clang-format" },
				cmake = { "cmake_format" },
				cpp = { "clang-format" },
				html = { "prettier" },
				json = { "prettier" },
				lua = { "stylua" },
				markdown = { "prettier" },
				proto = { "clang-format" },
				python = { "isort", "black" },
				ruby = { "rubocop" },
				rust = { "rustfmt" },
				sh = { "shfmt" },
				sql = { "pg_format" },
				tex = { "latexindent" },
				toml = { "taplo" },
				yaml = { "prettier" },
				["_"] = { "trim_whitespace" },
			},
		},
	},
	{
		"neovim/nvim-lspconfig",
		dependencies = {
			{ "j-hui/fidget.nvim", opts = {} },
			{ "folke/neodev.nvim", opts = {} },
		},
		config = function()
			vim.api.nvim_create_autocmd("LspAttach", {
				group = vim.api.nvim_create_augroup("lsp-attach", { clear = true }),
				callback = function(event)
					vim.keymap.set("n", "<leader>ld", vim.lsp.buf.definition, { desc = "find definition" })
					vim.keymap.set("n", "<leader>lD", vim.lsp.buf.declaration, { desc = "find declaration" })
					vim.keymap.set("n", "<leader>lh", vim.lsp.buf.hover, { desc = "show documentation" })
					vim.keymap.set(
						"n",
						"<leader>l/",
						require("telescope.builtin").lsp_references,
						{ desc = "Find references" }
					)
					vim.keymap.set("n", "<leader>lc", vim.lsp.buf.rename, { desc = "rename symbol" })

					local client = vim.lsp.get_client_by_id(event.data.client_id)
					if client and client.server_capabilities.documentHighlightProvider then
						vim.api.nvim_create_autocmd({ "CursorHold", "CursorHoldI" }, {
							buffer = event.buf,
							callback = vim.lsp.buf.document_highlight,
						})

						vim.api.nvim_create_autocmd({ "CursorMoved", "CursorMovedI" }, {
							buffer = event.buf,
							callback = vim.lsp.buf.clear_references,
						})
					end
				end,
			})

			local capabilities = vim.lsp.protocol.make_client_capabilities()
			capabilities = vim.tbl_deep_extend("force", capabilities, require("cmp_nvim_lsp").default_capabilities())

			local servers = {
				clangd = {
					filetypes = { "c", "cpp" },
				},
				rust_analyzer = {},
				bashls = {},
				texlab = {},
				pylsp = {
					settings = {
						pylsp = {
							plugins = {
								pylint = { enabled = true },
								flake8 = { enabled = true },
								pydocstyle = { enabled = true },
							},
						},
					},
				},
				lua_ls = {
					settings = {
						Lua = {
							completion = {
								callSnippet = "Replace",
							},
						},
					},
				},
				buf_ls = {},
			}

			for server, setup in pairs(servers) do
				require("lspconfig")[server].setup(setup)
			end
		end,
	},
	{
		"hrsh7th/nvim-cmp",
		event = "InsertEnter",
		dependencies = {
			{
				"L3MON4D3/LuaSnip",
				build = (function()
					return "make install_jsregexp"
				end)(),
				dependencies = {
					{
						"rafamadriz/friendly-snippets",
						config = function()
							require("luasnip.loaders.from_vscode").lazy_load()
							require("luasnip.loaders.from_snipmate").lazy_load()
						end,
					},
				},
			},
			"saadparwaiz1/cmp_luasnip",
			"hrsh7th/cmp-nvim-lsp",
			"hrsh7th/cmp-path",
		},
		config = function()
			local cmp = require("cmp")
			local luasnip = require("luasnip")
			luasnip.config.setup({})

			cmp.setup({
				snippet = {
					expand = function(args)
						luasnip.lsp_expand(args.body)
					end,
				},
				completion = { completeopt = "menu,menuone,noinsert" },

				mapping = cmp.mapping.preset.insert({
					["<C-n>"] = cmp.mapping.select_next_item(),
					["<C-p>"] = cmp.mapping.select_prev_item(),
					["<C-b>"] = cmp.mapping.scroll_docs(-4),
					["<C-f>"] = cmp.mapping.scroll_docs(4),
					["<CR>"] = cmp.mapping.confirm({ select = true }),
					["<C-Space>"] = cmp.mapping.complete({}),

					["<Tab>"] = cmp.mapping(function()
						if luasnip.expand_or_locally_jumpable() then
							luasnip.expand_or_jump()
						end
					end, { "i", "s" }),
					["<S-Tab>"] = cmp.mapping(function()
						if luasnip.locally_jumpable(-1) then
							luasnip.jump(-1)
						end
					end, { "i", "s" }),
				}),
				sources = {
					{ name = "nvim_lsp" },
					{ name = "luasnip" },
					{ name = "path" },
				},
			})
		end,
	},
	{
		"savq/melange-nvim",
		priority = 1000,
		init = function()
			vim.cmd.colorscheme("melange")
			vim.opt.termguicolors = true
		end,
	},
	{
		"nvim-treesitter/nvim-treesitter",
		build = ":TSUpdate",
		opts = {
			ensure_installed = {
				"bash",
				"c",
				"html",
				"lua",
				"luadoc",
				"markdown",
				"vim",
				"vimdoc",
				"rust",
				"python",
				"query",
			},
			auto_install = true,
			highlight = {
				enable = true,
				additional_vim_regex_highlighting = { "ruby" },
			},
			indent = { enable = true, disable = { "ruby" } },
		},
		config = function(_, opts)
			require("nvim-treesitter.configs").setup(opts)
		end,
	},
	{
		"mbbill/undotree",
		config = function()
			vim.keymap.set("n", "<leader>au", vim.cmd.UndotreeToggle, { desc = "undotree" })
		end,
	},
	{
		"echasnovski/mini.nvim",
		config = function()
			require("mini.ai").setup({ n_lines = 500 })
			require("mini.surround").setup()
		end,
	},
	{
		"nvim-lualine/lualine.nvim",
		dependencies = { "nvim-tree/nvim-web-devicons" },
		config = function()
			require("lualine").setup({
				options = {
					icons_enabled = true,
					theme = "auto",
					component_separators = { left = "", right = "" },
					section_separators = { left = "", right = "" },
					disabled_filetypes = {
						statusline = {},
						winbar = {},
					},
					ignore_focus = {},
					always_divide_middle = true,
					globalstatus = false,
					refresh = {
						statusline = 1000,
						tabline = 1000,
						winbar = 1000,
					},
				},
				sections = {
					lualine_a = { "mode" },
					lualine_b = { "branch", "diff", "diagnostics" },
					lualine_c = { "filename" },
					lualine_x = { "encoding", "fileformat", "filetype" },
					lualine_y = { "progress" },
					lualine_z = { "location" },
				},
				inactive_sections = {
					lualine_a = {},
					lualine_b = {},
					lualine_c = { "filename" },
					lualine_x = { "location" },
					lualine_y = {},
					lualine_z = {},
				},
				tabline = {},
				winbar = {},
				inactive_winbar = {},
				extensions = {},
			})
		end,
	},
	{
		"tpope/vim-fugitive",
		dependencies = { "ahmedkhalf/project.nvim" },
		config = function()
			vim.keymap.set("n", "<leader>pv", vim.cmd.Git, { desc = "fugitive" })
		end,
	},
	{
		"folke/todo-comments.nvim",
		event = "VimEnter",
		dependencies = { "nvim-lua/plenary.nvim" },
		opts = { signs = false },
	},

	{ "seanbreckenridge/yadm-git.vim" },
	{
		"https://git.sr.ht/~swaits/scratch.nvim",
		lazy = true,
		keys = {
			{ "<leader>bs", "<cmd>Scratch<cr>", desc = "Scratch Buffer", mode = "n" },
			{ "<leader>bS", "<cmd>ScratchSplit<cr>", desc = "Scratch Buffer (split)", mode = "n" },
		},
		cmd = {
			"Scratch",
			"ScratchSplit",
		},
		opts = {},
	},
	{
		"ThePrimeagen/refactoring.nvim",
		dependencies = {
			"nvim-lua/plenary.nvim",
			"nvim-treesitter/nvim-treesitter",
		},
		lazy = false,
		config = function()
			require("refactoring").setup({
				prompt_func_return_type = {
					c = true,
					cpp = true,
				},
				prompt_func_param_type = {
					c = true,
					cpp = true,
				},
			})
		end,
	},
})
