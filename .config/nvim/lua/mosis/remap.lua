vim.g.mapleader = " "
vim.g.maplocalleader = ","

vim.keymap.set("n", "<leader>ws", vim.cmd.vsplit, { desc = "split vertically" })
vim.keymap.set("n", "<leader>wS", vim.cmd.split, { desc = "split horizontally" })
vim.keymap.set("n", "<leader>wq", vim.cmd.quit, { desc = "close window" })

vim.keymap.set("n", "<leader>wh", function()
	vim.cmd.wincmd("h")
end, { desc = "select window right" })
vim.keymap.set("n", "<leader>wj", function()
	vim.cmd.wincmd("j")
end, { desc = "select window down" })
vim.keymap.set("n", "<leader>wk", function()
	vim.cmd.wincmd("k")
end, { desc = "select window up" })
vim.keymap.set("n", "<leader>wl", function()
	vim.cmd.wincmd("l")
end, { desc = "select window left" })

vim.keymap.set("n", "<leader>bn", vim.cmd.bnext, { desc = "buffer next" })
vim.keymap.set("n", "<leader>bp", vim.cmd.bprevious, { desc = "buffer previous" })

vim.keymap.set("n", "<leader>fs", vim.cmd.write, { desc = "save file" })
