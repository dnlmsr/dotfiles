vim.keymap.set("n", "<Tab>", "<Plug>fugitive:=", { buffer = true })

vim.keymap.set("n", "ff", function()
	vim.cmd.Git("fetch --all --prune")
end, { buffer = true })

vim.keymap.set("n", "pp", function()
	vim.cmd.Git("push")
end, { buffer = true })

vim.keymap.set("n", "la", function()
	vim.cmd.Git("log --graph --abbrev --oneline --all --decorate")
end, { buffer = true })
